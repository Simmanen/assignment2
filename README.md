# Lost In Translation

A react app that takes an input and converts it to sign language. The user has it own profile page where you can see your 10 latest translations, clear history and logout.

## Install

To run this project you have to:

### Clone the repository to your local machine

git clone https://gitlab.com/Simmanen/assignment2.git


### Install the necessary packages

npm install

### Start the development server

npm start
## Usage

Enter you name and press continue. Go to translation page when you get navigated to profile. On the translate page enter what you want translated. After some translations you can check your 10 latest translations on the profile page. Here you can also clear you translation history or logout.

## Contributers

Joachim Noor Atamna
Simen Heiestad Mandt

## Misq
Component Tree can be found in this root directory named "ComponentTree.pdf".




