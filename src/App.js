import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Navbar from './components/navbar/Navbar';
import LoginPage from './pages/LoginPage';
import ProfilePage from './pages/ProfilePage';
import TranslationPage from './pages/TranslationPage';


function App() {

  return (
    
    <BrowserRouter>
        <div className='App'>
          <Navbar/>
          <Routes>
            <Route path="/" element={<LoginPage/>}/>
            <Route path="/translation" element={<TranslationPage/>}/>
            <Route path="/profile" element={<ProfilePage/>}/>
          </Routes>
             <div className="area">
                <ul className="circles">
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                </ul>
            </div> 
        </div>
      </BrowserRouter>  
  );
}

export default App;
