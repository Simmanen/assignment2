import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL

//Adds the translation to the user profile
export const translateAdd = async (user, translate) => {
    try {
        const response= await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translate]
            })
        })

        if (!response.ok){
            throw new Error('Could not update the translation')
        }
        const result = await response.json()

        return [ null, result ]
    }
    catch (error) {
        return [error.message, null]
    }
}

//Clears the user's translation history and updates it with an empty array
export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })

        }
    )
    if(!response.ok){
        throw new Error('Could not update translations')
    }
    const result = await response.json()
    return [ null, result]
    } catch (error) {
        return[error.message, null]
    }
}