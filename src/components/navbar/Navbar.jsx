import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import "../stylesheets/Navbar.css";

const Navbar = () => {
    const { user } = useUser()
    return (
        <nav className="navbar"> 
            <h1>LOST IN TRANSLATION</h1>
            <img src="img/Logo.png" alt="logo" className="logo"/>
            { user !== null &&
                <div className="link-container">
                    <NavLink className="nav-links" to="profile">Profile</NavLink>
                    <NavLink className="nav-links" to="/translation">Translate</NavLink>
                </div>      
            }
        </nav>
    )
}
export default Navbar