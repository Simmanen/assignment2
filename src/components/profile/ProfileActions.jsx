import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete,storageSave } from "../../utils/storage"
import { translationClearHistory } from "../../api/translate"
import "../stylesheets/ProfileActions.css"

// Removes user information from storageand logs out the user
const ProfileActions = () => {

    const {user, setUser} = useUser()

    const handleLogout = () =>{
        storageDelete(STORAGE_KEY_USER)
        setUser(null)
    }
// Handle deletion of translation history
    const handleDeleteTranslations = async () => {
        if(!window.confirm('Are you sure?\nThis can not be undone!')){
            return
        }

        const [clearError] = await translationClearHistory(user.id)

        if (clearError !== null){
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return(
        <ul>
            <li><button className="btn-profile" onClick={handleDeleteTranslations}>Clear history</button></li>
            <li><button className="btn-profile" onClick={handleLogout}>Logout</button></li>
        </ul>
    )
}
export default ProfileActions
