import "../stylesheets/ProfileHeader.css";

const ProfileHeader = ({username}) => {
    return(
        <div className='wlcmMsg'><h1>Hello, user {username}</h1></div>
    )
}
export default ProfileHeader