import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import "../stylesheets/ProfileTranslationHistory.css"

// Displaying last 10 translations in a list with images for each letter
// Iterates through the elements which has a string, assigning image to each letter
const ProfileTranslationHistory = ({ translations }) => {

    const translateList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={index + '-' + translation} translation={translation}/>)
    return(
        <div className="profileHstry-container">
            <h1>Your translations: </h1>
            {translateList.slice(-10).map((item, index) => 
              <div style={{background: "#FFC75F"}}>
                <h2 style={{textAlign: 'center'}}>{item.props.translation}</h2>
                <div className="profile-forms-images">
                <ul key={index}>
                  {item.props.translation.split('').map((letter) => {
                    const source = letter === ' ' ? '/img/empty.png' : `/img/${letter}.png`;
                    return <img className="profile-img-translations" key={letter} src={source} alt={letter} />
                  })}
                </ul>
                </div>
              </div>
            )}
        </div>
    )
}
export default ProfileTranslationHistory