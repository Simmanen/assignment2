import { useState,useEffect } from 'react'
import {useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate} from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import "../stylesheets/Loginform.css";

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    //hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    // Local state
    const [ loading, setLoading ] =useState(false)
    const [ apiError, setApiError] = useState(null)

    // Side effect, Navigates to profile when user is not null
    useEffect(() => {
        if (user !== null){
            navigate('translation')
        }
    }, [ user, navigate ])

    // Handles the form submission, logs in user and saves it to the database
    const handleLogin = async ({username}) => {
        setLoading(true)
        const result = await loginUser(username);
        const [error, userResponse] = result;
        if (error) {
        setApiError(error);
        } else {
        storageSave(STORAGE_KEY_USER, userResponse);
        setUser(userResponse);
        }
        setLoading(false);
        };
        
        const onSubmit = handleSubmit(handleLogin);

    //Renders an error message based on the username data
    const getErrorMessage = (type) => {
        if(type === 'required') return 'Username is required';
        if(type === 'minLength') return 'Username is too short, minimum 3 characters';
        }
        const errorMessage =
         !errors.username ? null : <span className='status-message'>{getErrorMessage(errors.username.type)}</span>

    return(
            <form className="loginForm" onSubmit={ handleSubmit(onSubmit)}>
                <div className="form-input-container">
                    <input className='form-input' type="text" placeholder='Your name to login'{...register("username", usernameConfig)}/>
                    <div className='underline'></div>
                    {errorMessage}
                </div>
                <button type='submit' className='form-btn-submit'  disabled={ loading }>Continue<div className="cover"></div></button>
                {loading && <p className='status-message'>Logging in...</p>}
                {apiError && <p className='status-message'>{ apiError }</p>}
            </form>
    )
}
export default LoginForm;
