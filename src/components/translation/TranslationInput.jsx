import { useForm } from "react-hook-form"
import "../stylesheets/TranslationInput.css";

// Form component for submitting translation notes with validation
const TranslationForm = ({ onTranslate }) => {
    
    const { register, handleSubmit } = useForm()

    const onSubmit = ({ translateNotes }) => { 
        if(!(translateNotes === "" || translateNotes == null)){
            onTranslate(translateNotes.toLowerCase());
        }
    }

    return (
        <>
        <form className="translation-form" onSubmit={ handleSubmit(onSubmit) }>
            <input className="translation-input" id="input-translation" type="text" placeholder="Write here" { ...register('translateNotes') } />
            <button type="translation-submit" className="translation-btn-submit">Translate</button>
            <div className="cover"></div>
        </form>
        </>
    )
}
export default TranslationForm