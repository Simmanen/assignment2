import "../stylesheets/TranslationOutput.css";

// Renders translation output with image for each letter.
// Iterates through each letter in string and prints corresponding image.

const TranslationOutput = ({translation}) => {

  return (
      <div className="form-images" id="output-box">
        {translation.split('').map((letter, index) => {
          const source = letter === ' ' ? '/img/empty.png' : `/img/${letter}.png`;
          return <img className="img-translation" key={index} src={source} alt={letter} />
        })}
      </div>
  )
}

export default TranslationOutput;