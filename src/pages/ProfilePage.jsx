import ProfileActions from "../components/profile/ProfileActions";
import ProfileHeader from "../components/profile/ProfileHeader";
import ProfileTranslationHistory from "../components/profile/ProfileTranslationHistory";
import withAuth from "../components/withAuth";
import { useUser } from "../context/UserContext";

const ProfilePage = () => {

    const {user} = useUser()

    return (
        <>
            <ProfileHeader username={user.username}/>
            <ProfileActions/>
            <ProfileTranslationHistory translations={user.translations}/>
        </>
    )
}



export default withAuth(ProfilePage);