import { translateAdd } from "../api/translate";
import TranslationForm from "../components/translation/TranslationInput";
import TranslationOutput from "../components/translation/TranslationOutput";
import withAuth from "../components/withAuth";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import { storageSave } from "../utils/storage";
import React, { useState } from 'react';
import "../components/stylesheets/inputOutputContainer.css";

// Handles translation submit to add translation string to user
const TranslationPage = () => {

    const { user, setUser } = useUser()
    const [translation, setTranslation] = useState('')

    const handleTranslateClicked = async notes => {
        console.log(notes)
        const translation = notes

        const [error, updatedUser] = await translateAdd(user, translation)
        if (error !== null) {
            return
        }

        storageSave(STORAGE_KEY_USER, updatedUser)

        setUser(updatedUser)
        console.log('Error', error)
        console.log('Result', updatedUser)
        setTranslation(translation)   
    }

    return (
        <div className="container">
            <TranslationForm onTranslate={handleTranslateClicked}/>
            <TranslationOutput translation={translation}/>
        </div>
    )
}

export default withAuth(TranslationPage);
